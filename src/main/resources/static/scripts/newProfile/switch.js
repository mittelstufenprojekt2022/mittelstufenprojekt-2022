function nextPage() {
    for (var i = 1; i < 4; i++) {
        let row = document.getElementById(i);
        let nextRow = document.getElementById(i+1);
        if (row.classList.contains('active')) {
            row.classList.remove('active');
            nextRow.classList.add('active');
            break;
        }
    }
}

function previewPage() {
    for (var i = 4; i > 1; i--) {
        let row = document.getElementById(i);
        let previewRow = document.getElementById(i-1);
        if (row.classList.contains('active')) {
            row.classList.remove('active');
            previewRow.classList.add('active');
            break;
        }
    }

}