$(document).ready(function() {
    $.getJSON('../scripts/newProfile/city.json', function (city) {
        for (var i in city) {
            for (var t in city[i]) {
                let div = document.createElement('div');
                let item = document.createElement('p');
                let checkbox = document.createElement('input');
                checkbox.type= 'checkbox';
                checkbox.id = city[i][t]["id"];
                item.innerText = city[i][t]["name"];
                checkbox.setAttribute("value", city[i][t]["id"]);
                checkbox.name="searchCity";
                div.appendChild(checkbox);
                div.appendChild(item);
                if (t<7) {
                    document.querySelector('.line1').append(div);
                }
                else if (t>6 && t<13) {
                    document.querySelector('.line2').append(div);
                }
                else if (t>12) {
                    document.querySelector('.line3').append(div);
                }
            }
        }
    });
});