$(document).ready(function() {
    $.getJSON('../scripts/newProfile/hobby.json', function (data) {
        for (var i in data) {
            var block = document.createElement('div');
            document.querySelector('.hobbyListItemSearch').appendChild(block);
            for (var t in data[i]) {
                let div = document.createElement('div');
                let item = document.createElement('p');
                let img = document.createElement('img');
                let checkbox = document.createElement('input');
                checkbox.type= 'checkbox';
                checkbox.id = data[i][t]["id"];
                item.innerText = data[i][t]["name"];
                img.src = data[i][t]["img"];
                img.title = data[i][t]["name"];
                div.appendChild(checkbox);
                div.appendChild(img);
                div.appendChild(item);
                if (t == 0) {div.classList.add('first');}
                block.appendChild(div);
            }
        }
    });
});