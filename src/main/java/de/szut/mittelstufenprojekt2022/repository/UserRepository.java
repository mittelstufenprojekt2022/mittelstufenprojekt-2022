package de.szut.mittelstufenprojekt2022.repository;

import de.szut.mittelstufenprojekt2022.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    //@Query("SELECT * FROM Match m WHERE m.id = :id")
   // List<Match> findAllActiveUsers( @Param("id") int id);

    //List<Match> findAllByMe_Id(int id);

    List<User> findAllByAgeAndCity(int age, String City);
}
