package de.szut.mittelstufenprojekt2022.repository;

import de.szut.mittelstufenprojekt2022.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchRepository extends JpaRepository<Match, Integer> {

    //@Query("SELECT * FROM Match m WHERE m.id = :id")
   // List<Match> findAllActiveUsers( @Param("id") int id);

    //List<Match> findAllByMe_Id(int id);

    List<Match>findAllByMeId(int otherId);



    @Query("SELECT m FROM Match m " +
            "JOIN User u ON (u.id = m.otherId)" +
            "JOIN u.hobbies hob" +
        //    " JOIN Category c ON(c.id = hob.id)" +
            " WHERE hob.name = :catergory")
    List<Match> findMatchesByHobby(
            @Param("catergory") String catergory);




}
