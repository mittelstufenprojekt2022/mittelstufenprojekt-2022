package de.szut.mittelstufenprojekt2022.repository;

import de.szut.mittelstufenprojekt2022.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {


    public Category findByName(String name);


}
