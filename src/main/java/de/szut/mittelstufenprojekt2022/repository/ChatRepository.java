package de.szut.mittelstufenprojekt2022.repository;

import de.szut.mittelstufenprojekt2022.model.ChatMassage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatRepository extends JpaRepository<ChatMassage, Integer> {
    List<ChatMassage> findAllBySenderAndConsignee(int sender , int consignee);

    void deleteAllBySenderAndConsignee(int sender, int consignee);

}

