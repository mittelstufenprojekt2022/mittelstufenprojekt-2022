package de.szut.mittelstufenprojekt2022.repository;

import de.szut.mittelstufenprojekt2022.model.UserLike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserLikeRepository extends JpaRepository<UserLike, Integer> {
    @Query("SELECT ul FROM UserLike ul WHERE ul.meId = ?1")
    List<UserLike> findAllByMeId(Integer meId);

    @Query("SELECT ul FROM UserLike ul WHERE ul.userId = ?1")
    List<UserLike> findAllByUserId(Integer userId);
}
