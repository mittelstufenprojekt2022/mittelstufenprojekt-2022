package de.szut.mittelstufenprojekt2022.repository;

import de.szut.mittelstufenprojekt2022.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Integer> {
}
