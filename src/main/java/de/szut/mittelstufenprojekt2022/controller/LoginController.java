package de.szut.mittelstufenprojekt2022.controller;

import de.szut.mittelstufenprojekt2022.model.Person;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.repository.PersonRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Controller
public class LoginController {

    @Autowired
    private PersonRepository repository;

    @PostMapping("/login")
    public String login(
            @RequestParam(name = "username") String name,
            @RequestParam(name = "password") String password) {
        System.out.println(name + password);
       List<Person> personList = repository.findAll();

        for (Person user : personList) {
            if (user.getUsername().toLowerCase().contains(name.toLowerCase())
                    && user.getPasswort().equals(password)) {
                return "redirect:/homepage";
            }
        }
        return "/login";
    }

    @GetMapping(value = "/login")
    public String saveNewProfile (Model model){
        model.addAttribute("user", new User());
        return "/login";
    }
}
