package de.szut.mittelstufenprojekt2022.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class FooterController {

    @GetMapping("/Datenschutz")
    public String datenschutz() {

        return "datenschutz";
    }

    @GetMapping("/Impressum")
    public String impressum() {

        return "impressum";
    }
}
