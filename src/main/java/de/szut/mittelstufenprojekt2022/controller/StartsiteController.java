package de.szut.mittelstufenprojekt2022.controller;

import de.szut.mittelstufenprojekt2022.model.Category;
import de.szut.mittelstufenprojekt2022.model.Match;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.model.UserLike;
import de.szut.mittelstufenprojekt2022.repository.CategoryRepository;
import de.szut.mittelstufenprojekt2022.repository.MatchRepository;
import de.szut.mittelstufenprojekt2022.repository.UserLikeRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller
public class StartsiteController {
    @Autowired
    private UserRepository repository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserLikeRepository userLikeRepository;

    @Autowired
    private MatchRepository matchRepo;

    private List<String> cities;

    private final ArrayList<Integer> age;

    private List<User> searchedUser;

    public StartsiteController(ArrayList<String> cities, ArrayList<Integer> age) {

        this.cities = cities;
        this.age = age;

    }

    @PostMapping("/homepage/like")
    public String like(@RequestParam(name = "userId") int id, Model model) {

        int meId = 9;

        System.out.println(id);

        UserLike ul = new UserLike();

        ul.setUserId(id);

        ul.setMeId(meId);

        userLikeRepository.save(ul);

        List<UserLike> otherLikesObject = userLikeRepository.findAllByUserId(id);

        for (UserLike otherli : otherLikesObject) {

            System.out.println("z" + otherli.getMeId()

            );

        }

        List<Integer> otherLikeIds = new ArrayList<>();

        for (UserLike otherl : otherLikesObject) {

            otherLikeIds.add(otherl.getMeId());

        }

        for (int otherli : otherLikeIds) {

            System.out.println(otherli);

        }

        if (otherLikeIds.contains(meId)) {

            System.out.println("kiuztsthdtu");

            Match newMatch = new Match();

            newMatch.setMeId(meId);

            newMatch.setOtherId(id);

            matchRepo.save(newMatch);

        }

        System.out.println("rf");

        return "redirect:/homepage";

    }

    @GetMapping("/homepage")
    public String profile(Model model) {

        int meId = 9;

        List<User> allUser = repository.findAll();
        List<Category> allHobbies = categoryRepository.findAll();
        List<UserLike> likedUser = userLikeRepository.findAllByMeId(9);
        List<Integer> likedUserIds = new ArrayList<>();


        for (UserLike userLike : likedUser) {
            likedUserIds.add(userLike.getUserId());
        }

        Random random = new Random();
        int index = 0;
        ArrayList<Integer> userIDs = new ArrayList<>();
        for (int i = 0; i < allUser.size(); i++) {
            if (allUser.get(i).getId() != meId) {
                if (!likedUserIds.contains(allUser.get(i).getId())) {
                    userIDs.add(allUser.get(i).getId());
                }
            }
        }

        for (int i = 0; i < allUser.size(); i++) { // hier überprüfen, dass die Städte nicht wiederholt vorkommen!
            cities.add(allUser.get(i).getCity());
            //index = random.nextInt(userIDs.size());
        }

        cities = cities.stream().distinct().collect(Collectors.toList());
        index = random.nextInt(userIDs.size());
        User user = new User();
        if (searchedUser== null) {
            user = repository.findById(userIDs.get(index)).get();
        } else {
            for (User newUser : searchedUser) {
                int id = newUser.getId();
                user = repository.findById(id).get();

            }
        }


        for (int i = 18; i < 100; i++) {
            age.add(i);
        }

        model.addAttribute("hobbies", allHobbies);
        model.addAttribute("cities", cities);
        model.addAttribute("ages", age);
        model.addAttribute("profile", user);

        return "homepage";

    }

    @PostMapping("/homepage/search")
    public String search(@RequestParam(name = "age") int age, @RequestParam(name = "city") String city,
                         @RequestParam(name = "hobby") String hobby, Model model) {


        searchedUser = repository.findAllByAgeAndCity(age, city);


        return "redirect:/homepage";
    }

}

