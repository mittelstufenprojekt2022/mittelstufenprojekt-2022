package de.szut.mittelstufenprojekt2022.controller;

import de.szut.mittelstufenprojekt2022.model.Category;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.repository.CategoryRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class newProfileController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private CategoryRepository categoryRepository;

    @PostMapping("/newProfile")
    public String setNewProfile(
            @RequestParam(name="username") String username,
            @RequestParam(name="age") int age,
            @RequestParam(value = "city") String city,
            @RequestParam (value = "hobby") int[] hobbyId,
            @RequestParam (name = "description") String description
            /*@RequestParam(name = "searchAgeMin") int searchAgeMin,
            @RequestParam(name = "searchAgeMax") int searchAgeMax,
            @RequestParam(value = "searchHobby") int[] searchHobbyId,
            @RequestParam (value = "gender") String gender,
            @RequestParam(value = "searchCity") int cityId,
            @RequestParam (name = "password") String password,
            @RequestParam (value = "myGender") String myGender*/){
        if(age < 18 ) {
            return "newProfile";
        }

        ArrayList<Category> hobbies = new ArrayList<>();
        for (int h : hobbyId) {
            Category category = categoryRepository.findById(h).get();
            hobbies.add(category);
        }

      /*   ArrayList<Category> searchHobbies = new ArrayList<>();
        for (int hobby : searchHobbyId) {
         Category categorys = categoryRepository.findById(hobby).get();
            searchHobbies.add(categorys);
         System.out.println(hobby);
        }*/

        User user = new User(
                username,
                age,
                city,
                hobbies,
                description
                /*searchAgeMin,
                searchAgeMax,
                hobbies,
                gender,
                cityId,
                password,
                myGender*/);

       // repository.save(user);

        return "/homepage";
    }

    @GetMapping(value = "/newProfile")
    public String saveNewProfile(Model model) {
        model.addAttribute("user", new User());
        return "/newProfile";
    }

}
