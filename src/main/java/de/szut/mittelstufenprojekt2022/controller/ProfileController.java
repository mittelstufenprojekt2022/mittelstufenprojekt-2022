package de.szut.mittelstufenprojekt2022.controller;

import de.szut.mittelstufenprojekt2022.model.Category;
import de.szut.mittelstufenprojekt2022.model.City;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.repository.CategoryRepository;
import de.szut.mittelstufenprojekt2022.repository.CityRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProfileController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CityRepository cityRepository;

    @GetMapping("/profile")
    public String profile(Model model) {

        int meId = 9;
        User testuser = userRepository.findById(meId).get();

        List<Category> categories = getCategories(testuser);



        //Städteobjekt
        ArrayList<String> options = new ArrayList<>();
        List<City> cities = cityRepository.findAll();
        for (City city : cities) {
            options.add(city.getName());
        }

        model.addAttribute("UserAufrufen", testuser);
        model.addAttribute("categories", categories);
        model.addAttribute("options", options);
        System.out.println("test3");
        return "profile";
    }

    private List<Category> getCategories(User testuser) {
        List<Integer> userHobby = new ArrayList<>();
        List<Category> userHobbies = testuser.getHobbies();
        for (Category category : userHobbies) {
            userHobby.add(category.getId());
        }
        List<Category> categories = new ArrayList<>();
        List<Category> categoryList = categoryRepository.findAll();
        for (Category category : categoryList) {
            int hobbyId = category.getId();
            for (int i = 0; i < userHobby.size(); i++) {
                int id = userHobbies.get(i).getId();
                category.setChecked(hobbyId == id);
            }
            if (userHobbies.isEmpty()) {
                category.setChecked(false);
            }
            categories.add(category);

        }
        return categories;
    }

    @PostMapping("/profile/updateUser")
    public String updateUser(@RequestParam(name = "name") String name,
                             @RequestParam(name = "city") String city,
                             @RequestParam(name = "description") String description){
                            // @RequestParam(name = "hobbies") List<Category> hobbyList) {
        System.out.println(name);
        System.out.println(city);
        System.out.println(description);
        //System.out.println(hobbyList);
        int meId = 9;
        User user = userRepository.findById(meId).get();
        if (!name.isEmpty()) {
            user.setUsername(name);
        } else {
            String userName = user.getUsername();
            user.setUsername(userName);
        }
        if (!city.isEmpty()) {
            user.setCity(city);
        } else {
            String userCity = user.getCity();
            user.setCity(userCity);
        }
        if (!description.isEmpty()) {
            user.setDescription(description);
        } else {
            String userDescription = user.getDescription();
            user.setDescription(userDescription);
        }
        //user.setHobbies(hobbyList);
        userRepository.save(user);


        return "redirect:/profile";
    }
}
