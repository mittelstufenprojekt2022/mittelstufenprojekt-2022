package de.szut.mittelstufenprojekt2022.controller;

import de.szut.mittelstufenprojekt2022.DTO.MatchDTO;
import de.szut.mittelstufenprojekt2022.model.Category;
import de.szut.mittelstufenprojekt2022.model.Match;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.repository.CategoryRepository;
import de.szut.mittelstufenprojekt2022.repository.ChatRepository;
import de.szut.mittelstufenprojekt2022.repository.MatchRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import de.szut.mittelstufenprojekt2022.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MatchesController {

    @Autowired
    private MatchService service;

    @Autowired
    private MatchRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private CategoryRepository cat;

    @GetMapping("/matches")
    public String matches(Model model) {

        // Category cae = cat.findById(2).get();
        //  List<Category> hob = new ArrayList<>();
        //  hob.add(cae);

        //   user4.setHobbies(hob);

        //   userRepository.save(user4);

        int meid = 9;
        User meUser = userRepository.findById(meid).get();

        ArrayList<String> categories = new ArrayList<String>();
        List<Match> matches = repository.findAllByMeId(meUser.getId());

        List<MatchDTO> matchesDTOList = service.matchModelListToDTO(matches);

        for (Category c : meUser.getHobbies()) {
            categories.add(c.getName());
        }

        model.addAttribute("categories", categories);
        if (matchesDTOList.size() > 0) {
            model.addAttribute("matchesList", matchesDTOList);
        }

        return "matches";
    }

    @GetMapping("/matches/{category}")
    public String filterForCategory(@PathVariable(value = "category") String category, Model model) {
        if (category.equals("all")) {
            return "redirect:/matches";
        } else {
            Category c = cat.findByName(category);
            int meid = 9;
            User meUser = userRepository.findById(meid).get();
            List<Match> matches = repository.findAllByMeId(meUser.getId());
            if (matches.size() > 0) {
                for (int i = 0; i < matches.size(); i++) {
                    User other = userRepository.findById(matches.get(i).getOtherId()).get();
                    if (!other.getHobbies().contains(c)) {
                        matches.remove(matches.get(i));
                    }
                }

                List<MatchDTO> matchesDtoList = service.matchModelListToDTO(matches);

                if (matchesDtoList.size() > 0) {
                    model.addAttribute("matchesList", matchesDtoList);
                }
            }
            ArrayList<String> categories = new ArrayList<String>();
            for (Category cat : meUser.getHobbies()) {
                categories.add(cat.getName());
            }
            model.addAttribute("categories", categories);

        }
        return "matches";
    }

    @PostMapping("/matches/delete")
    public String delete(@RequestParam int id, @RequestParam(value = "meId") int meId, @RequestParam(value = "otherId") int otherId, Model model) {
        try {
            repository.deleteById(id);
            chatRepository.deleteAllBySenderAndConsignee(meId, otherId);
            chatRepository.deleteAllBySenderAndConsignee(otherId, meId);
        } catch (Exception e) {
            model.addAttribute("errorMessage", "Match konnte nicht aufgelöst werden!");
            return "redirect:/matches";
        }

        model.addAttribute("successMessage", "Match wurde erfolgreich aufgelöst!");
        return "redirect:/matches";
    }

    @GetMapping("/matchDetails/{id}")
    public String showDetail(@PathVariable(value = "id") int matchId, Model model) {
        Match match = null;
        try {
            match = repository.getById(matchId);
        } catch (EntityNotFoundException e) {
            model.addAttribute("EntityNotFound", "Entity not found");
            return "matchdetail";
        }
        User matchUser = null;

        try {
            matchUser = userRepository.findById(match.getOtherId()).get();
        } catch (EntityNotFoundException e) {
            model.addAttribute("EntityNotFound", "Entity not found");
            return "matchdetail";
        }

        if (matchUser != null) {
            model.addAttribute("matchUser", matchUser);
        } else {
            model.addAttribute("EntityNotFound", "Entity not found");
        }
        return "matchdetail";

    }
}