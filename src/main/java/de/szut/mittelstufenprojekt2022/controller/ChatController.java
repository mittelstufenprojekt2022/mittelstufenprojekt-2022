package de.szut.mittelstufenprojekt2022.controller;

import de.szut.mittelstufenprojekt2022.DTO.MatchDTO;
import de.szut.mittelstufenprojekt2022.model.ChatMassage;
import de.szut.mittelstufenprojekt2022.model.Match;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.repository.ChatRepository;
import de.szut.mittelstufenprojekt2022.repository.MatchRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import de.szut.mittelstufenprojekt2022.service.ChatService;
import de.szut.mittelstufenprojekt2022.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private MatchService matchService;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MatchRepository matchRepository;


    private String updateChat(HttpServletRequest httpServletRequest, String massage) {
        String referer = httpServletRequest.getHeader("referer");
        return "redirect:" + referer;
    }

    @PostMapping("/chat/massage")
    public String getChatMassage(HttpServletRequest httpServletRequest,
                                 @RequestParam(value = "massage") String massage,
                                 @RequestParam(value = "id") int otherId) {
        int meId = 9;
        String timestamp = new SimpleDateFormat("dd.MM.yy HH:mm").format(new Date());
        try {
            chatRepository.save(new ChatMassage(massage, meId, timestamp, otherId, true));
        } catch (Exception e){
            System.out.println("Chatmassage Speichern fehlgeschlagen");
        }
        return updateChat(httpServletRequest, massage);
    }

    @GetMapping(value = "/chat/massage")
    public String loadChat() {
        return "/chat";
    }

    @GetMapping("/chat")
    public String welcome(Model model) {

        int meId = 9;
        User meUser = userRepository.findById(meId).get();

        List<Match> matches = matchRepository.findAllByMeId(meUser.getId());
        List<MatchDTO> matchesDTOList = matchService.matchModelListToDTO(matches);

        String text = "Chats";
        model.addAttribute("SiteName", text);
        model.addAttribute("profils", matchesDTOList);
        return "chat";
    }

    @GetMapping("/chat/{id}")
    public String getChatById(@PathVariable(value = "id") int matchId, Model model) {

        int meId = 9;
        User meUser = userRepository.findById(meId).get();
        User otherUser = userRepository.findById(matchId).get();

        List<Match> matches = matchRepository.findAllByMeId(meUser.getId());
        List<MatchDTO> matchesDTOList = matchService.matchModelListToDTO(matches);
        List<ChatMassage> consigneeSendMassages = chatRepository.findAllBySenderAndConsignee(matchId, meUser.getId());
        List<ChatMassage> meSendMassages = chatRepository.findAllBySenderAndConsignee(meUser.getId(), matchId);
        List<ChatMassage> chatMassages = new ArrayList<>();
        chatMassages.addAll(consigneeSendMassages);
        chatMassages.addAll(meSendMassages);
        for (ChatMassage chatMassage : chatMassages) {
            if (chatMassage.getConsignee() == matchId) {
                chatMassage.setMeSendMessage(true);
            } else {
                chatMassage.setMeSendMessage(false);
            }
        }

        model.addAttribute("chatMassages", chatMassages);
        model.addAttribute("username", otherUser.getUsername());
        model.addAttribute("matchId", otherUser.getId());
        model.addAttribute("profils", matchesDTOList);
        return "chat";
    }

}
