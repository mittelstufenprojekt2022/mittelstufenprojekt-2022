package de.szut.mittelstufenprojekt2022.service;

import de.szut.mittelstufenprojekt2022.repository.ChatRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import de.szut.mittelstufenprojekt2022.model.ChatMassage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatService {

    @Autowired
    ChatRepository repository;

    @Autowired
    UserRepository userRepository;

    public ChatMassage create(ChatMassage model){
        return repository.save(model);
    }

    public ChatMassage findById(int id) {
        ChatMassage model = (repository.findById(id)).get();
        return model;

    }
}
