package de.szut.mittelstufenprojekt2022.service;

import de.szut.mittelstufenprojekt2022.DTO.MatchDTO;
import de.szut.mittelstufenprojekt2022.model.Match;
//import de.szut.mittelstufenprojekt2022.repository.MatchRepository;
import de.szut.mittelstufenprojekt2022.model.User;
import de.szut.mittelstufenprojekt2022.repository.MatchRepository;
import de.szut.mittelstufenprojekt2022.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MatchService {

    @Autowired
    MatchRepository repository;

    @Autowired
    UserRepository userRepository;

    public Match create(Match model) {
        return repository.save(model);
    }

    public Match findById(int id) {
        Match model = (repository.findById(id)).get();
        return model;

    }

    public List<Match> findAll() {
        return repository.findAll();

    }

    public List<MatchDTO> matchModelListToDTO(List<Match> matchList) {
        List<MatchDTO> matchDtoList = new ArrayList<>();
        for(Match model : matchList) {
            MatchDTO dto = new MatchDTO();
            dto.setId(model.getId());

            User me = userRepository.findById(model.getMeId()).get();
            dto.setMe(me);

            User other = userRepository.findById(model.getOtherId()).get();
            dto.setOther(other);

            matchDtoList.add(dto);
        }

        return matchDtoList;
    }
}