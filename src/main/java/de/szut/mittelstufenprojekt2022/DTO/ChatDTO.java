package de.szut.mittelstufenprojekt2022.DTO;

import de.szut.mittelstufenprojekt2022.model.User;
import lombok.Data;


@Data
public class ChatDTO {
    int id;
    User me;
    User other;
}
