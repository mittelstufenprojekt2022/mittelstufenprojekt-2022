package de.szut.mittelstufenprojekt2022.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data

public class UserLike {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "userId")
    int userId;
    @Column(name = "meId")
    int meId;

    public UserLike() {

    }
}
