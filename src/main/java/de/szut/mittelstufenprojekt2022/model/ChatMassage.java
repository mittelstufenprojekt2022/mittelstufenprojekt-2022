package de.szut.mittelstufenprojekt2022.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Table(name = "chatMassage")
public class ChatMassage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "massage")
    String massage;

    @Column(name = "sender")
    int sender;

    @Column(name = "timestamp")
    String timestamp;

    @Column(name = "consignee")
    int consignee;

    @Column(name = "meSendMessage")
    private boolean meSendMessage;


    public ChatMassage(String massage, int sender, String timestamp, int consignee, boolean meSendMessage) {
        this.massage = massage;
        this.sender = sender;
        this.timestamp = timestamp;
        this.consignee = consignee;
        this.meSendMessage = meSendMessage;
    }



}
