package de.szut.mittelstufenprojekt2022.model;

import lombok.Data;

@Data
public class Profile {
    private String userName;
    private String profileBild;
    private String name;
    private int age;
    private String city;
    private String info;

    public Profile(String userName, String profileBild, String name, int age, String city, String info) {
        this.userName = userName;
        this.profileBild = profileBild;
        this.name = name;
        this.age = age;
        this.city = city;
        this.info = info;
    }
}
