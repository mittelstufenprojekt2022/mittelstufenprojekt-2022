package de.szut.mittelstufenprojekt2022.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Match {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    //@JoinColumn(name = "User", nullable = false)
    //@OneToOne (mappedBy = "Match")l
    //@ManyToOne(fetch = FetchType.EAGER, optional = false)


    //@OneToOne(fetch = FetchType.EAGER, optional = false)
    //@JoinColumn(name = "id", insertable=false,  updatable=false) // referencedColumnName = "idModel"
    //User me;

   // @JoinColumn(name = "User", nullable = false)


   //@OneToOne(fetch = FetchType.EAGER, optional = false)
   //@JoinColumn(name = "user_id", insertable=false,  updatable=false)
    //User other;

    @Column(name="meId")
    int meId;
    @Column(name="otherId")
    int otherId;



    public Match(int id, int meId, int otherId) {
        this.id = id;
        this.meId = meId;
        this.otherId = otherId;

       // this.meId = me.getId();
       // this.otherId = other.getId();

        //this.me = me;
        //this.other = other;
    }

    public Match() {

    }

    //public Match getOther() {
    //}

}
