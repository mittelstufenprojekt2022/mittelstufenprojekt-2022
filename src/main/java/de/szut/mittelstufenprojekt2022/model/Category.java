package de.szut.mittelstufenprojekt2022.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "Category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String name;

    @ManyToMany(mappedBy = "hobbies", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<User> users;

    String imgName;

    boolean isChecked;

    public Category (int id, String name){
        this.id = id;
        this.name = name;
    }
    public Category (int id, String name, String imgName, boolean isChecked){
        this.id = id;
        this.name = name;
    }


    public Category() {

    }

}
