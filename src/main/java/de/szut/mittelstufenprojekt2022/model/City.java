package de.szut.mittelstufenprojekt2022.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "City")
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String name;
}
