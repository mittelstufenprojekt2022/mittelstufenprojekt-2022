package de.szut.mittelstufenprojekt2022.model;


import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", unique = true)
    int id;

    @Column(name="username")
    String username;

    @Column(name="age")
    int age;

    @Column(name="city")
    String city;

   // @Column(name = "password")
   // String password;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
            @JoinTable(
                    name = "user_category",
                    joinColumns = @JoinColumn(name = "user_id"),
                    inverseJoinColumns = @JoinColumn(name = "category_id")
            )
    List<Category> hobbies;

    @Column(name="description")
    String description;

   /* @Column (name="searchAgeMin")
    int searchAgeMin;

    @Column (name="searchAgeMax")
    int searchAgeMax;

    @Column(name = "gender")
    String gender;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
            @JoinTable(
                    name = "search_category",
                    joinColumns = @JoinColumn(name = "user_id"),
                    inverseJoinColumns = @JoinColumn(name = "searchcategory_id")
            )
    List<Category> searchHobbies;

    @Column(name="searchCity")
    int searchCity;

    @Column(name = "myGender")
    String myGender;

   public User(
            String username,
            int age,
            String city,
            List<Category> hobbies,
            String description
            /*int searchAgeMin,
            int searchAgeMax,
            List<Category> searchHobbies,
            String gender,
            int searchCity,
            String password,
            String myGender) {

        this.username = username;
        this.age = age;
        this.city = city;
        this.hobbies = hobbies;
        this.description = description;
        this.searchAgeMin = searchAgeMin;
        this.searchAgeMax = searchAgeMax;
        this.searchHobbies = searchHobbies;
        this.gender = gender;
        this.searchCity = searchCity;
        this.password = password;
        this.myGender = myGender;
    }*/

//    public User() {

  //  }

    public User(String username, int age, String city, List<Category> hobbies, String description) {
        this.username = username;
        this.age = age;
        this.city = city;
        this.hobbies = hobbies;
        this.description = description;
    }

    public User() {

    }

   /* public User(
            String username,
            String password) {

        this.username = username;
        this.password = password;
    }*/
}
