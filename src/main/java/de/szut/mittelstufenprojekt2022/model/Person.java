package de.szut.mittelstufenprojekt2022.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    private String username;
    private String passwort;


    public Person(String username, String passwort) {
        this.passwort=passwort;
        this.username=username;
    }

    public Person () {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }
}
