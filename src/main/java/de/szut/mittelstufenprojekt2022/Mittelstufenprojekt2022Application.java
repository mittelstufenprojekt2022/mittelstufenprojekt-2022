package de.szut.mittelstufenprojekt2022;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mittelstufenprojekt2022Application {

	public static void main(String[] args) {
		SpringApplication.run(Mittelstufenprojekt2022Application.class, args);
	}

}
